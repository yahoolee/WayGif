package com.way.test;

import java.io.IOException;

import android.app.Activity;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.widget.ImageView;

import com.way.gif.AnimationListener;
import com.way.gif.GifDrawable;
import com.way.gif.R;

public class MainActivity extends Activity implements AnimationListener{
	private ImageView mGifImageView;
	private ImageView mGifImageView1;
	private GifDrawable gifDrawable;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mGifImageView = (ImageView) findViewById(R.id.gif_img);
		mGifImageView1 = (ImageView) findViewById(R.id.gif_img1);
		
		try {
			gifDrawable = new GifDrawable(getResources(), R.drawable.biz_plugin_weather_baoyu);
			gifDrawable.addAnimationListener(this);
			//gifDrawable.setAutoMirrored(mirrored)
			mGifImageView.setImageDrawable(gifDrawable);
			GifDrawable gifDrawable1 = new GifDrawable(getResources(), R.drawable.f025);
			mGifImageView1.setImageDrawable(gifDrawable1);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onAnimationCompleted() {
		// TODO Auto-generated method stub
		gifDrawable.reset();
		gifDrawable.start();
	}

}
